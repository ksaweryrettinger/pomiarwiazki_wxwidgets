﻿#include "cMain.h"

wxBEGIN_EVENT_TABLE(cMain, wxFrame)
	EVT_BUTTON(10001, cMain::OnButtonClickedWsun)
	EVT_BUTTON(10002, cMain::OnButtonClickedWysun)
	EVT_BUTTON(10003, cMain::OnButtonClickedCalibrate)
	EVT_MENU(wxID_PREFERENCES, cMain::OnMenuClickedDiagnostics)
	EVT_MENU(wxID_INFO, cMain::OnMenuClickedInformation)
	EVT_MENU(wxID_EXIT, cMain::OnMenuClickedExit)
	EVT_TIMER(10004, cMain::OnWriteTimer)
	EVT_TIMER(10008, cMain::OnMotorFailTimer)
	EVT_CLOSE(cMain::OnClose)
wxEND_EVENT_TABLE()

/* Frame constructor */
cMain::cMain(wxString* mbPort, int32_t mbAddress, uint8_t mbSlaveNum, wxString* mFrameTitle) :
             wxFrame(nullptr, wxID_ANY, " ", wxPoint(0, 130), wxSize(710, 630),
                     (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
	//Shift window
	this->SetPosition(this->GetPosition() + ((mbSlaveNum - 1) * wxPoint(720, 0)));

    //Set window background colour
    this->SetBackgroundColour(wxColour(230, 230, 230));

    //Set frame mFrameTitle
    this->SetTitle(*mFrameTitle + " (0x0" + wxString::Format(wxT("%i"), mbAddress) + ")");

	//Store Modbus connection settings
	this->mbPort = mbPort;
	this->mbSlaveNum = mbSlaveNum;
	this->mbAddress = mbAddress;

	//Pointer initialization
	mMenuBar = nullptr;
	mToolsMenu = nullptr;
	mHelpMenu = nullptr;
	btnWsun = nullptr;
	btnWysun = nullptr;
	pngHandler = nullptr;
	bgImage = nullptr;
	txtLocked = nullptr;
	txtMotorFail = nullptr;
	txtReferenceADC = nullptr;
	txtADC1 = nullptr;
	txtADC2 = nullptr;
	txtADC3 = nullptr;
	txtModbusErrorRead = nullptr;
	txtErrorADC = nullptr;
	mbContext = nullptr;
	mFrameDiag = nullptr;

	//Variables initialization
	mbADCErrorsRegister = 0;

	//Modbus status flags initialization
	mbStatusConnected = -1;
	mbStatusRead = -1;
	mbStatusDiagnostics = -1;
	mbStatusDiagnosticsReset = -1;

	//Other flags initialization
	mbModeWriteCoil = -1;
	mbSafetyLockTriggered = false;
	mbCoilWriteComplete = false;
	mbDiagnosticsEnabled = false;
	mbDiagnosticsAvailable = false;
	mbDiagnosticsResetEnabled = false;
	mbDiagnosticsResetting = false;
	mbDiagnosticsOpenWindow = false;
	mbDiagnosticsWindowIsOpen = false;
	informationWindowIsOpen = false;

	//Create buttons
	btnWsun = new wxButton(this, 10001, _T("Wsuń"), wxPoint(550, 420), wxSize(140, 50));
	btnWysun = new wxButton(this, 10002, _T("Wysuń"), wxPoint(550, 480), wxSize(140, 50));
    btnCalibrate = new wxButton(this, 10003, _T("Kalibracja"), wxPoint(550, 360), wxSize(140, 50));

    myFont = wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
    btnWsun->SetFont(myFont);
    btnWysun->SetFont(myFont);
    btnCalibrate->SetFont(myFont);

	if (btnWsun != nullptr) btnWsun->SetBackgroundColour(wxColour(210, 210, 210));
	if (btnWysun != nullptr) btnWysun->SetBackgroundColour(wxColour(210, 210, 210));
	if (btnCalibrate != nullptr) btnCalibrate->SetBackgroundColour(wxColour(210, 210, 210));

    //Disable buttons
    if (btnWsun->IsEnabled()) btnWsun->Disable();
    if (btnWysun->IsEnabled()) btnWysun->Disable();
    if (btnCalibrate->IsEnabled()) btnCalibrate->Disable();

	//Add background images
	pngHandler = new wxPNGHandler;
	bgImage = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
	                                                              wxString("/eclipse-workspace/PomiarWiazki_wxWidgets/Graphics/reference.png"),
	                                                              wxBITMAP_TYPE_PNG), wxPoint(550, 260), wxSize(140, 48));
	if (mbSlaveNum == 1)
	{
	bgImage = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
	                                                              wxString("/eclipse-workspace/PomiarWiazki_wxWidgets/Graphics/concentric-circles-v2.png"),
	                                                              wxBITMAP_TYPE_PNG), wxPoint(20, 30), wxSize(510, 510));
	}
	else
	{
	    bgImage = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
	                                                                  wxString("/eclipse-workspace/PomiarWiazki_wxWidgets/Graphics/concentric-circles.png"),
	                                                                  wxBITMAP_TYPE_PNG), wxPoint(20, 30), wxSize(510, 510));
	}
	imageLocked = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
	                                                                  wxString("/eclipse-workspace/PomiarWiazki_wxWidgets/Graphics/locked2.png"),
	                                                                  wxBITMAP_TYPE_PNG), wxPoint(590, 50), wxSize(54, 54));
    imageLockFail = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
                                                                      wxString("/eclipse-workspace/PomiarWiazki_wxWidgets/Graphics/lockfail2.png"),
                                                                      wxBITMAP_TYPE_PNG), wxPoint(590, 50), wxSize(56, 56));
	imageMotorFail = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
	                                                                  wxString("/eclipse-workspace/PomiarWiazki_wxWidgets/Graphics/motorfail2.png"),
	                                                                  wxBITMAP_TYPE_PNG), wxPoint(580, 40), wxSize(70, 68));
    imageConnectionError = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
                                                                      wxString("/eclipse-workspace/PomiarWiazki_wxWidgets/Graphics/connectionerror.png"),
                                                                      wxBITMAP_TYPE_PNG), wxPoint(590, 50), wxSize(54, 54));

    //Menu bar
    mMenuBar = new wxMenuBar();

    //Tools menu
    mToolsMenu = new wxMenu();
    if (mToolsMenu != nullptr)
    {
        mToolsMenu->Append(wxID_PREFERENCES, _T("&Diagnostyka"));
        mToolsMenu->AppendSeparator();
        mToolsMenu->Append(wxID_EXIT, _T("&Wyjdź"));
        mMenuBar->Append(mToolsMenu, _T("&Narzędzia"));
    }

    //About menu
    mHelpMenu = new wxMenu();
    if (mHelpMenu != nullptr)
    {
        mHelpMenu->Append(wxID_INFO, _T("&Informacje"));
        mMenuBar->Append(mHelpMenu, _T("&Pomoc"));
    }

    SetMenuBar(mMenuBar);

	//Initialize static texts
    txtLocked = new wxStaticText(this, wxID_ANY, _T("Blokada Aktywna"), wxPoint(550, 110), wxSize(140, 20), wxALIGN_CENTRE_HORIZONTAL);
    txtLockFail = new wxStaticText(this, wxID_ANY, _T("Błąd Blokady"), wxPoint(550, 110), wxSize(140, 20), wxALIGN_CENTRE_HORIZONTAL);
    txtMotorFail = new wxStaticText(this, wxID_ANY, _T("Błąd Napędu"), wxPoint(550, 110), wxSize(140, 20), wxALIGN_CENTRE_HORIZONTAL);
	txtReference = new wxStaticText(this, wxID_ANY, _T("Pomiar Referencyjny"), wxPoint(550, 240), wxSize(140, 20), wxALIGN_CENTRE_HORIZONTAL);
    txtReferenceADC = new wxStaticText(this, wxID_ANY, " N/A", wxPoint(560, 270), wxSize(110, 30), wxALIGN_CENTRE_HORIZONTAL);

    if (mbSlaveNum == 1)
    {
        txtADC1 = new wxStaticText(this, wxID_ANY, "N/A", wxPoint(220, 80), wxSize(110, 30), wxALIGN_CENTRE_HORIZONTAL);
        txtADC2 = new wxStaticText(this, wxID_ANY, "N/A", wxPoint(220, 470), wxSize(110, 30), wxALIGN_CENTRE_HORIZONTAL);
    }
    else
    {
        txtADC1 = new wxStaticText(this, wxID_ANY, "N/A", wxPoint(220, 70), wxSize(110, 30), wxALIGN_CENTRE_HORIZONTAL);
        txtADC2 = new wxStaticText(this, wxID_ANY, "N/A", wxPoint(220, 155), wxSize(110, 30), wxALIGN_CENTRE_HORIZONTAL);
    }

	txtADC3 = new wxStaticText(this, wxID_ANY, "N/A", wxPoint(220, 270), wxSize(110, 30), wxALIGN_CENTRE_HORIZONTAL);

	if (*mbPort == wxString("Port Error")) txtModbusErrorRead = new wxStaticText(this, wxID_ANY, _T("Błąd Modbus:\nPort Zajęty"), wxPoint(560, 110), wxSize(100, 10), wxALIGN_CENTRE_HORIZONTAL);
    else txtModbusErrorRead = new wxStaticText(this, wxID_ANY, _T("Błąd Modbus:\nBrak Połączenia"), wxPoint(560, 110), wxSize(100, 10), wxALIGN_CENTRE_HORIZONTAL);

	if (txtLocked != nullptr) txtLocked->SetForegroundColour(wxColour(207, 31, 37)); //red
	if (txtLockFail != nullptr) txtLockFail->SetForegroundColour(wxColour(255, 136, 0)); //orange
	if (txtMotorFail != nullptr) txtMotorFail->SetForegroundColour(wxColour(255, 136, 0)); //orange
	if (txtModbusErrorRead != nullptr) txtModbusErrorRead->SetForegroundColour(wxColour(207, 31, 37)); //red

	if (mbSlaveNum == 1)
	{
	     if (txtADC1 != nullptr) txtADC1->SetBackgroundColour(wxColour(0, 170, 250));
	     if (txtADC2 != nullptr) txtADC2->SetBackgroundColour(wxColour(0, 170, 250));
	     if (txtADC3 != nullptr) txtADC3->SetBackgroundColour(wxColour(98, 204, 254));
	}
	else
	{
	    if (txtADC1 != nullptr) txtADC1->SetBackgroundColour(wxColour(0, 170, 250));
	    if (txtADC2 != nullptr) txtADC2->SetBackgroundColour(wxColour(98, 204, 254));
	    if (txtADC3 != nullptr) txtADC3->SetBackgroundColour(wxColour(214, 242, 254));
	}

	//Set text and message fonts
    txtReference->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
    txtMotorFail->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_SEMIBOLD));
    txtLockFail->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_SEMIBOLD));
	txtLocked->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_SEMIBOLD));
	txtModbusErrorRead->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_SEMIBOLD));

	//Set ADC readings font
	if (txtReferenceADC != nullptr) txtReferenceADC->SetFont(wxFont(18, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_SEMIBOLD));
	if (txtADC1 != nullptr) txtADC1->SetFont(wxFont(18, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	if (txtADC2 != nullptr) txtADC2->SetFont(wxFont(18, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	if (txtADC3 != nullptr) txtADC3->SetFont(wxFont(18, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

	//Hide all user messages
	DisplayUserMessage(0, 0, 0, 0);

	//Create timers
	mbWriteTimer = new wxTimer(this, 10004);
	tMotorFailTimer = new wxTimer(this, 10008);

	//Initialize Modbus communication
    mbContext = modbus_new_rtu(mbPort->mb_str(), MB_BAUD, MB_PARITY, MB_NUM_BITS, MB_NUM_STOP);
	modbus_set_slave(mbContext, mbAddress);
	modbus_rtu_set_serial_mode(mbContext, MODBUS_RTU_RS232);
	modbus_set_response_timeout(mbContext, 1, 0);
	modbus_set_byte_timeout(mbContext, 0, 100000);
	mbStatusConnected = modbus_connect(mbContext);

	//Update raw diagnostics messages with correct slave address
	mbDiagRawRequest[0] = (uint8_t) mbAddress;
	mbDiagResetRawRequest[0] = (uint8_t) mbAddress;

	if (mbStatusConnected == -1) DisplayUserMessage(1, -1, -1, -1); //show connection error

    //Bind helper thread callback function
    Bind(wxEVT_THREAD, &cMain::OnThreadUpdate, this);

	//Start Modbus communication
	StartModbusThread();
}

void cMain::OnMenuClickedDiagnostics(wxCommandEvent& evt)
{
	if (mbDiagnosticsWindowIsOpen) //window already open
	{
		mFrameDiag->Raise(); //show window
	}
	else
	{
		if (mToolsMenu->IsEnabled(wxID_PREFERENCES)) mToolsMenu->Enable(wxID_PREFERENCES, false);
		mbDiagnosticsWindowIsOpen = true;
		mbDiagnosticsOpenWindow = true; //signal helper thread to open diagnostics window
		mbDiagnosticsEnabled = true; //signal helper thread to start reading diagnostic data
	}

	evt.Skip();
}

void cMain::OnMenuClickedInformation(wxCommandEvent& evt)
{
	if (informationWindowIsOpen) //window already open
	{
		mFrameInfo->Raise(); //show window
	}
	else
	{
		informationWindowIsOpen = true;
		mFrameInfo = new cInformation(this);
		mFrameInfo->SetPosition(this->GetPosition());
		mFrameInfo->SetIcon(this->GetIcon());
		mFrameInfo->Show();
	}

	evt.Skip();
}

/* "Wsun" button callback */
void cMain::OnButtonClickedWsun(wxCommandEvent& evt)
{
	if (btnWysun->IsEnabled()) //only respond when other button has not been pressed
	{
		btnWsun->Disable();
		if (!mbSafetyLockTriggered) mbModeWriteCoil = 1; //set flag, event handled in Modbus thread
	}

	evt.Skip();
}

/* "Wysun" button callback */
void cMain::OnButtonClickedWysun(wxCommandEvent& evt)
{
	if (btnWsun->IsEnabled()) //only respond when other button has not been pressed
	{
		btnWysun->Disable();
		if (!mbSafetyLockTriggered) mbModeWriteCoil = 0; //set flag, event handled in Modbus thread
	}

	evt.Skip();
}

/* "Kalibracja" button callback */
void cMain::OnButtonClickedCalibrate(wxCommandEvent& evt)
{
    for (uint8_t i = 1; i < MB_NUM_ADC_REG; i++)
    {
        f_mbADCOffsets[i] = f_mbADCRegisters[i];
    }

    evt.Skip();
}

/* Start of Modbus helper thread */
void cMain::StartModbusThread()
{
	if (CreateThread(wxTHREAD_JOINABLE) != wxTHREAD_NO_ERROR)
	{
		wxLogError("Could not create the worker thread!");
		return;
	}
	if (GetThread()->Run() != wxTHREAD_NO_ERROR)
	{
		wxLogError("Could not run the worker thread!");
		return;
	}
}

/* Modbus helper thread */
wxThread::ExitCode cMain::Entry()
{
	while (!GetThread()->TestDestroy())
	{
		{
			//Enter critical section
			wxCriticalSectionLocker lock(ModbusCS);

			//Attempt reconnection
			if (mbStatusConnected == -1) mbStatusConnected = modbus_connect(mbContext);

			//Read input registers
			mbStatusRead = modbus_read_input_registers(mbContext, 3001, int32_t(MB_NUM_ADC_REG), mbADCRegisters);
			mbADCErrorsRegister = mbADCRegisters[MB_NUM_ADC_REG - 1];

			for (uint8_t i = 0; i < MB_NUM_ADC_REG; i++)
			{
				if (mbSlaveNum == 1) f_mbADCRegisters[i] = ((float)mbADCRegisters[i])/ADC_SCALING_FACTOR_CORRECTED;
				else f_mbADCRegisters[i] = ((float)mbADCRegisters[i])/ADC_SCALING_FACTOR;

				if (i > 0)
				{
				    f_mbADCRegisters[i] = ((f_mbADCRegisters[i] - f_mbADCOffsets[i]) > 0) ? (f_mbADCRegisters[i] - f_mbADCOffsets[i]) : 0;
				}
			}

			wxThread::Sleep(20); //20ms delay

			//Read diagnostics
			if (mbDiagnosticsEnabled)
			{
                if (mbStatusConnected != -1)
                {
                    modbus_send_raw_request(mbContext, mbDiagRawRequest, 6 * sizeof(uint8_t));
                    mbStatusDiagnostics = modbus_receive_raw_confirmation(mbContext, mbDiagRawResponse);
                    wxThread::Sleep(20); //20ms delay

                    //Update diagnostics
                    if (mbStatusDiagnostics != -1)
                    {
                        for (uint8_t i = 0; i < MB_NUM_DIAG; i++)
                        {
                            mbDiagnosticsRegister[i] = (uint16_t)mbDiagRawResponse[i * 2 + 4] << 8;
                            mbDiagnosticsRegister[i] |= (uint16_t)mbDiagRawResponse[i * 2 + 5];
                        }
                    }
                }

				if (!mbDiagnosticsAvailable) mbDiagnosticsAvailable = true;
			}

			//Reset diagnostics
			if (mbDiagnosticsResetEnabled)
			{
				//Send diagnostics reset request
				modbus_send_raw_request(mbContext, mbDiagResetRawRequest, 6 * sizeof(uint8_t));
				mbStatusDiagnosticsReset = modbus_receive_raw_confirmation(mbContext, mbDiagRawResponse);
				wxThread::Sleep(20); //20ms delay

				//Clear diagnostic register in preparation for new data
				if (mbStatusDiagnosticsReset != -1)
				{
					for (uint8_t i = 0; i < MB_NUM_DIAG; i++)
					{
						mbDiagnosticsRegister[i] = 0;
					}
				}

				//Clear reset flags
				mbDiagnosticsResetting = false;
				mbDiagnosticsResetEnabled = false;
			}

			//Write to configuration coil
			if (mbModeWriteCoil != -1)
			{
				modbus_write_bit(mbContext, 1, mbModeWriteCoil);
				wxThread::Sleep(20); //20ms delay
				mbCoilWriteComplete = true;
			}

            //Read coils
            modbus_read_bits(mbContext, 1, 3, mbCoils);

			//Signal main thread that new data has been received
			wxQueueEvent(GetEventHandler(), new wxThreadEvent());
		}

		//Delay next Modbus message cycle
		wxThread::Sleep(MB_SCAN_RATE);
	}

	//Destroy thread
	return (wxThread::ExitCode)0;
}

/* Event handler for the Modbus helper thread */
void cMain::OnThreadUpdate(wxThreadEvent& evt)
{
	//Enter critical section
	wxCriticalSectionLocker lock(ModbusCS);

	//Open diagnostics window
	if (mbDiagnosticsOpenWindow && mbDiagnosticsAvailable)
	{
		mFrameDiag = new cDiagnostics(this);
		mFrameDiag->SetPosition(this->GetPosition());
		mFrameDiag->SetIcon(this->GetIcon());
		mFrameDiag->Show();
		mbDiagnosticsOpenWindow = false;
	}

	if ((mbStatusConnected != -1) && (mbStatusRead != -1)) //Modbus Connection OK
	{
        //Check coil status and update user messages
        if (mbCoils[1]) //safety lock enabled
        {
            if (!mbSafetyLockTriggered)
            {
                mbSafetyLockTriggered = true;

                //New motor command, start timer
                if (tMotorFailTimer->IsRunning()) tMotorFailTimer->Stop();
                if (!(tMotorFailTimer->IsRunning())) tMotorFailTimer->Start(MOTOR_TIMEOUT, wxTIMER_ONE_SHOT);
            }
        }
        else if (!mbCoils[1]) //safety lock reset
        {
            if (mbSafetyLockTriggered)
            {
            	mbSafetyLockTriggered = false;

                //New motor command, start timer
                if (tMotorFailTimer->IsRunning()) tMotorFailTimer->Stop();
                if (!(tMotorFailTimer->IsRunning())) tMotorFailTimer->Start(MOTOR_TIMEOUT, wxTIMER_ONE_SHOT);

                //Hide lock confirmation and lock failure messages
                DisplayUserMessage(-1, -1, 0, 0);
            }
        }

        //New motor command sent
        if ((mbModeWriteCoil != -1) && mbCoilWriteComplete)
        {
            if (!mbSafetyLockTriggered)
            {
                //Start button pressed timer
                if (!(mbWriteTimer->IsRunning())) mbWriteTimer->Start(BUTTON_TIMEOUT, wxTIMER_ONE_SHOT);

                //New motor command, start timer
                if (tMotorFailTimer->IsRunning()) tMotorFailTimer->Stop();
                if (!(tMotorFailTimer->IsRunning())) tMotorFailTimer->Start(MOTOR_TIMEOUT, wxTIMER_ONE_SHOT);
            }

            //Reset flags
            mbModeWriteCoil = -1;
            mbCoilWriteComplete = false;
        }

        //Check motor status
        if (mbCoils[0] == mbCoils[2]) //motor OK
        {
            //Stop motor command timer
            if (tMotorFailTimer->IsRunning()) tMotorFailTimer->Stop();

            //Hide motor and lock errors
            DisplayUserMessage(-1, 0, 0, -1);

            if (mbSafetyLockTriggered)
            {
                //Show lock confirmation
                DisplayUserMessage(0, 0, 0, 1);
            }
        }
        else if (!(tMotorFailTimer->IsRunning()))
        {
        	if (mbSafetyLockTriggered) DisplayUserMessage(0, 0, 1, 0); //show lock error
        	else DisplayUserMessage(0, 1, 0, 0); //show motor error
        }

        //Update button colours based on motor status
        if (mbCoils[2])
        {
            if ((btnWsun->GetBackgroundColour()) != wxColour(0, 210, 0)) btnWsun->SetBackgroundColour(wxColour(0, 210, 0));
            if ((btnWysun->GetBackgroundColour()) != wxColour(210, 210, 210)) btnWysun->SetBackgroundColour(wxColour(210, 210, 210));
        }
        else
        {
            if ((btnWysun->GetBackgroundColour()) != wxColour(0, 210, 0)) btnWysun->SetBackgroundColour(wxColour(0, 210, 0));
            if ((btnWsun->GetBackgroundColour()) != wxColour(210, 210, 210)) btnWsun->SetBackgroundColour(wxColour(210, 210, 210));
        }

        // Modbus Connection OK, ADC Readings OK
        if (mbADCErrorsRegister == 0)
        {
            //Hide connection error
            DisplayUserMessage(0, -1, -1, -1);

            //Enable buttons
            if (!(mbWriteTimer->IsRunning()) && mbModeWriteCoil == -1)
            {
                if (!(btnWsun->IsEnabled())) btnWsun->Enable();
                if (!(btnWysun->IsEnabled())) btnWysun->Enable();
            }

            if (!(btnCalibrate->IsEnabled())) btnCalibrate->Enable();

            //Update static text
            if ((f_mbADCRegisters[0] < ADC_REFERENCE_MIN) || (f_mbADCRegisters[0] > ADC_REFERENCE_MAX))
            {
                txtReferenceADC->SetPosition(wxPoint(560, 270));
                txtReferenceADC->SetLabel(_T(" Błąd"));
            }
            else
            {
                txtReferenceADC->SetPosition(wxPoint(550, 270));
                txtReferenceADC->SetLabel(_T("  OK"));
            }

            txtADC1->SetLabel(wxString::Format(wxT("%0.1f"), f_mbADCRegisters[1]) + _T("μA"));
            txtADC2->SetLabel(wxString::Format(wxT("%0.1f"), f_mbADCRegisters[2]) + _T("μA"));
            txtADC3->SetLabel(wxString::Format(wxT("%0.1f"), f_mbADCRegisters[3]) + _T("μA"));
        }
        else // Modbus Connection OK, ADC Readings NOK
        {
            //Hide connection error
            DisplayUserMessage(0, -1, -1, -1);

            //Enable buttons
            if (!(mbWriteTimer->IsRunning()) && mbModeWriteCoil == -1)
            {
                if (!(btnWsun->IsEnabled())) btnWsun->Enable();
                if (!(btnWysun->IsEnabled())) btnWysun->Enable();
            }

            if (!(btnCalibrate->IsEnabled())) btnCalibrate->Enable();

            //Update static texts
            if (mbADCErrorsRegister >> 0 & 0x0001)
            {
                txtReferenceADC->SetPosition(wxPoint(560, 270));
                adcReferenceLabel = " N/A";
            }
            else if ((f_mbADCRegisters[0] < ADC_REFERENCE_MIN) || (f_mbADCRegisters[0] > ADC_REFERENCE_MAX))
            {
                txtReferenceADC->SetPosition(wxPoint(560, 270));
                adcReferenceLabel = _T(" Błąd");
            }
            else
            {
                txtReferenceADC->SetPosition(wxPoint(550, 270));
                adcReferenceLabel = "  OK";
            }

            adc1Label = ((mbADCErrorsRegister >> 1) & 0x0001) ? "N/A" : (wxString::Format(wxT("%0.1f"), f_mbADCRegisters[1]) + _T("μA"));
            adc2Label = ((mbADCErrorsRegister >> 2) & 0x0001) ? "N/A" : (wxString::Format(wxT("%0.1f"), f_mbADCRegisters[2]) + _T("μA"));
            adc3Label = ((mbADCErrorsRegister >> 3) & 0x0001) ? "N/A" : (wxString::Format(wxT("%0.1f"), f_mbADCRegisters[3]) + _T("μA"));

            txtReferenceADC->SetLabel(adcReferenceLabel);
            txtADC1->SetLabel(adc1Label);
            txtADC2->SetLabel(adc2Label);
            txtADC3->SetLabel(adc3Label);
        }
	}
	else //Modbus Connection NOK
	{
		//Stop timers
	    if (mbWriteTimer->IsRunning()) mbWriteTimer->Stop();
        if (tMotorFailTimer->IsRunning()) tMotorFailTimer->Stop();

        //Reset button colours
		if ((btnWysun->GetBackgroundColour()) != wxColour(210, 210, 210)) btnWysun->SetBackgroundColour(wxColour(210, 210, 210));
		if ((btnWsun->GetBackgroundColour()) != wxColour(210, 210, 210)) btnWsun->SetBackgroundColour(wxColour(210, 210, 210));

		//Show connection error
		DisplayUserMessage(1, 0, 0, 0);

		//Disable buttons
		if (btnWsun->IsEnabled()) btnWsun->Disable();
		if (btnWysun->IsEnabled()) btnWysun->Disable();
		if (btnCalibrate->IsEnabled()) btnCalibrate->Disable();

		//Update static text
		txtReferenceADC->SetPosition(wxPoint(560, 270));
		txtReferenceADC->SetLabel(" N/A");
		txtADC1->SetLabel("N/A");
		txtADC2->SetLabel("N/A");
		txtADC3->SetLabel("N/A");
	}
}

/* Timer event - Modbus write */
void cMain::OnWriteTimer(wxTimerEvent& evt)
{
	//Re-enable buttons
	if (!(btnWsun->IsEnabled())) btnWsun->Enable();
	if (!(btnWysun->IsEnabled())) btnWysun->Enable();
}

/* Timer event - motor error */
void cMain::OnMotorFailTimer(wxTimerEvent& evt)
{
    if (mbSafetyLockTriggered)
    {
        //Display lock failure
        DisplayUserMessage(0, 0, 1, 0);
    }
    else
    {
        //Display motor failure
        DisplayUserMessage(0, 1, 0, 0);
    }
}

/* Function for handling the display of user messages */
void cMain::DisplayUserMessage(int8_t bConnectionError, int8_t bMotorFail, int8_t bLockFail, int8_t bLocked)
{
    //Connection Errors
    if (bConnectionError == 1)
    {
        if (!(imageConnectionError->IsShown())) imageConnectionError->Show();
        if (!(txtModbusErrorRead->IsShown())) txtModbusErrorRead->Show();
    }
    else if (bConnectionError == 0)
    {
        if (imageConnectionError->IsShown()) imageConnectionError->Hide();
        if (txtModbusErrorRead->IsShown()) txtModbusErrorRead->Hide();
    }

    //Motor Errors
    if (bMotorFail == 1)
    {
        if (!(imageMotorFail->IsShown())) imageMotorFail->Show();
        if (!(txtMotorFail->IsShown())) txtMotorFail->Show();
    }
    else if (bMotorFail == 0)
    {
        if (imageMotorFail->IsShown()) imageMotorFail->Hide();
        if (txtMotorFail->IsShown()) txtMotorFail->Hide();
    }

    //Safety Lock Error
    if (bLockFail == 1)
    {
        if (!(imageLockFail->IsShown())) imageLockFail->Show();
        if (!(txtLockFail->IsShown())) txtLockFail->Show();
    }
    else if (bLockFail == 0)
    {
        if (imageLockFail->IsShown()) imageLockFail->Hide();
        if (txtLockFail->IsShown()) txtLockFail->Hide();
    }

    //Safety Lock Engaged
    if (bLocked == 1)
    {
        if (!(imageLocked->IsShown())) imageLocked->Show();
        if (!(txtLocked->IsShown())) txtLocked->Show();
    }
    else if (bLocked == 0)
    {
        if (imageLocked->IsShown()) imageLocked->Hide();
        if (txtLocked->IsShown()) txtLocked->Hide();
    }
}

/* Frame closed - Modbus thread destruction */
void cMain::OnClose(wxCloseEvent&)
{
	//Stop timers
	if (mbWriteTimer->IsRunning()) mbWriteTimer->Stop();
	if (tMotorFailTimer->IsRunning()) tMotorFailTimer->Stop();

	//Close diagnostics window and delete diagnostics thread
	if (mbDiagnosticsWindowIsOpen) mFrameDiag->Close();

	//Close information window
	if (informationWindowIsOpen) mFrameInfo->Close();

	//Check if thread exists and is running
	if (GetThread() && GetThread()->IsRunning()) GetThread()->Delete();

	//Close Modbus connection and free resources
	modbus_close(mbContext);
	modbus_flush(mbContext);
	modbus_free(mbContext);

	Destroy();
}

/* Frame closed from menu - Modbus thread destruction */
void cMain::OnMenuClickedExit(wxCommandEvent& evt)
{
	//Stop timers
	if (mbWriteTimer->IsRunning()) mbWriteTimer->Stop();
	if (tMotorFailTimer->IsRunning()) tMotorFailTimer->Stop();

	//Close diagnostics window and delete diagnostics thread
	if (mbDiagnosticsWindowIsOpen) mFrameDiag->Close();

	//Close information window
	if (informationWindowIsOpen) mFrameInfo->Close();

	//Check if thread exists and is running
	if (GetThread() && GetThread()->IsRunning()) GetThread()->Delete();

	//Close Modbus connection and free resources
	modbus_close(mbContext);
	modbus_flush(mbContext);
	modbus_free(mbContext);

	Destroy();
}
