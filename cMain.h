#pragma once

#include "modbus.h"
#include "wx/wx.h"
#include "cDiagnostics.h"
#include "cInformation.h"
#include "constants.h"

class cDiagnostics;
class cInformation;

class cMain : public wxFrame, public wxThreadHelper
{
public: //constructor
	cMain(wxString*, int32_t, uint8_t, wxString*);

protected: //helper thread
    virtual wxThread::ExitCode Entry();
    wxCriticalSection ModbusCS;

private: //event handlers
	void OnMenuClickedConnect(wxCommandEvent& evt);
	void OnMenuClickedDiagnostics(wxCommandEvent& evt);
	void OnMenuClickedInformation(wxCommandEvent& evt);
	void OnMenuClickedExit(wxCommandEvent& evt);
	void OnButtonClickedWsun(wxCommandEvent& evt);
	void OnButtonClickedWysun(wxCommandEvent& evt);
	void OnButtonClickedCalibrate(wxCommandEvent& evt);
	void OnWriteTimer(wxTimerEvent& evt);
	void OnMotorFailTimer(wxTimerEvent& evt);
	void OnThreadUpdate(wxThreadEvent& evt);
	void OnClose(wxCloseEvent& evt);
	void StartModbusThread();

private: //other functions
	void DisplayUserMessage(int8_t, int8_t, int8_t, int8_t);

private: //static texts and labels
	wxStaticText* txtLocked;
	wxStaticText* txtLockFail;
	wxStaticText* txtMotorFail;
    wxStaticText* txtReference;
    wxStaticText* txtReferenceADC;
    wxStaticText* txtADC1;
    wxStaticText* txtADC2;
    wxStaticText* txtADC3;
    wxStaticText* txtModbusErrorRead;
    wxStaticText* txtErrorADC;
	wxString adcReferenceLabel;
    wxString adc1Label;
    wxString adc2Label;
    wxString adc3Label;

private: //other wxWidgets components
    wxFont myFont;
    wxMenuBar* mMenuBar;
    wxButton* btnWsun;
    wxButton* btnWysun;
    wxButton* btnCalibrate;
    wxPNGHandler* pngHandler;
    wxStaticBitmap* bgImage;
    wxStaticBitmap* imageLocked;
    wxStaticBitmap* imageLockFail;
    wxStaticBitmap* imageMotorFail;
    wxStaticBitmap* imageConnectionError;
    wxTimer* mbWriteTimer;
    wxTimer* tMotorFailTimer;
    wxTimer* mLockFailTimer;

public: //variables used by other frames
    wxMenu* mToolsMenu;
    wxMenu* mHelpMenu;
    int32_t mbStatusDiagnostics;
    int32_t mbStatusDiagnosticsReset;
    uint16_t mbDiagnosticsRegister[MB_NUM_DIAG] = { 0 };
    bool mbDiagnosticsEnabled;
    bool mbDiagnosticsAvailable;
    bool mbDiagnosticsResetEnabled;
    bool mbDiagnosticsResetting;
    bool mbDiagnosticsWindowIsOpen;
    bool informationWindowIsOpen;

public: //Modbus configuration
    uint8_t mbSlaveNum;
    int32_t mbAddress;
    wxString* mbPort;
    modbus_t* mbContext;

private: //data buffers
	uint8_t mbCoils[MB_NUM_COILS] = { 0 };
	uint16_t mbADCErrorsRegister;
	uint16_t mbADCRegisters[MB_NUM_ADC_REG] = { 0 };
    uint8_t mbDiagRawRequest[MB_DIAG_RAW_LENGTH] = { 0x00, 0x08, 0x00, 0x02, 0x00, 0x00 };
    uint8_t mbDiagResetRawRequest[MB_DIAG_RAW_LENGTH] = { 0x00, 0x08, 0x00, 0x01, 0xFF, 0x00 };
	uint8_t mbDiagRawResponse[MODBUS_RTU_MAX_ADU_LENGTH] = { 0 };
    float f_mbADCRegisters[MB_NUM_ADC_REG] = { 0 };
    float f_mbADCOffsets[MB_NUM_ADC_REG] = {0};

private: //status flags
	int32_t mbStatusConnected;
	int32_t mbStatusRead;

private: //other flags
	int8_t mbModeWriteCoil;
    bool mbDiagnosticsOpenWindow;
    bool mbSafetyLockTriggered;
	bool mbCoilWriteComplete;

private: //frame pointers
    cDiagnostics* mFrameDiag;
    cInformation* mFrameInfo;

	wxDECLARE_EVENT_TABLE();
};

