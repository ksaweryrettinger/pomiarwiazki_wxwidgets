#pragma once

#include "wx/wx.h"
#include "cMain.h"

class cMain;

class cDiagnostics : public wxFrame, public wxThreadHelper
{
public: //constructor
	cDiagnostics(cMain*);

protected: //helper thread
    virtual wxThread::ExitCode Entry();
    wxCriticalSection csLock;

private: //event handlers
	void OnClose(wxCloseEvent& evt);
	void OnButtonClickedReset(wxCommandEvent& evt);
	void StartDiagnosticsThread();
	void OnThreadUpdate(wxThreadEvent& evt);
	void OnDiagnosticsResetTimer(wxTimerEvent& evt);

private: //static texts
    wxStaticText* txtDiagLabel1;
    wxStaticText* txtDiagLabel2;
    wxStaticText* txtDiagLabel3;
    wxStaticText* txtDiagLabel4;
    wxStaticText* txtDiagLabel5;
    wxStaticText* txtDiagLabel6;
    wxStaticText* txtDiagLabel7;
    wxStaticText* txtDiagLabel8;
    wxStaticText* txtDiag1;
    wxStaticText* txtDiag2;
    wxStaticText* txtDiag3;
    wxStaticText* txtDiag4;
    wxStaticText* txtDiag5;
    wxStaticText* txtDiag6;
    wxStaticText* txtDiag7;
    wxStaticText* txtDiag8;
    wxStaticText* txtDiagResetError;

private: //other components
    wxFont myFont;
    wxButton* btnReset;
    wxTimer* mbDiagnosticsResetTimer;

private: //variables
    bool mbDiagnosticsResetPressed;

private: //pointer received in constructor
    cMain* pMain;

	wxDECLARE_EVENT_TABLE();
};

