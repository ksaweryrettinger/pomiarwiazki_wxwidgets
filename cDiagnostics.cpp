#include "cDiagnostics.h"

wxBEGIN_EVENT_TABLE(cDiagnostics, wxFrame)
	EVT_TIMER(10006, cDiagnostics::OnDiagnosticsResetTimer)
    EVT_BUTTON(10007, cDiagnostics::OnButtonClickedReset)
	EVT_CLOSE(cDiagnostics::OnClose)
wxEND_EVENT_TABLE()

cDiagnostics::cDiagnostics(cMain* pMain) : wxFrame(nullptr, wxID_ANY, " ", wxPoint(200, 160), wxSize(440, 570), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
	Bind(wxEVT_THREAD, &cDiagnostics::OnThreadUpdate, this);

	//Set title
	this->SetTitle("Diagnostyka Modbus (0x0" + wxString::Format(wxT("%i"), pMain->mbAddress) + ")");

	//Pointer initialization
	btnReset = nullptr;
	txtDiagLabel1 = nullptr;
	txtDiagLabel2 = nullptr;
	txtDiagLabel3 = nullptr;
	txtDiagLabel4 = nullptr;
	txtDiagLabel5 = nullptr;
	txtDiagLabel6 = nullptr;
	txtDiagLabel7 = nullptr;
	txtDiagLabel8 = nullptr;
	txtDiag1 = nullptr;
	txtDiag2 = nullptr;
	txtDiag3 = nullptr;
	txtDiag4 = nullptr;
	txtDiag5 = nullptr;
	txtDiag6 = nullptr;
	txtDiag7 = nullptr;
	txtDiag8 = nullptr;

	//Flag initialization
	mbDiagnosticsResetPressed = false;

	//Store main window pointer
	this->pMain = pMain;

	//Set background colour
	this->SetBackgroundColour(wxColour(230, 230, 230));

	//Create diagnostics reset button
	btnReset = new wxButton(this, 10007, "Reset", wxPoint(180, 460), wxSize(100, 40));
	if (btnReset != nullptr) btnReset->SetBackgroundColour(wxColour(210, 210, 210));

	//Initialize diagnostic counter titles
	txtDiagLabel1 = new wxStaticText(this, wxID_ANY, "Bus Messages:             ", wxPoint(50, 50), wxSize(100, 20), wxALIGN_RIGHT);
	txtDiagLabel2 = new wxStaticText(this, wxID_ANY, "Bus Communication Errors: ", wxPoint(50, 100), wxSize(100, 20));
	txtDiagLabel3 = new wxStaticText(this, wxID_ANY, "Bus Exception Errors:     ", wxPoint(50, 150), wxSize(100, 20));
	txtDiagLabel4 = new wxStaticText(this, wxID_ANY, "Server Messages:          ", wxPoint(50, 200), wxSize(100, 20));
	txtDiagLabel5 = new wxStaticText(this, wxID_ANY, "Server No Response:       ", wxPoint(50, 250), wxSize(100, 20));
	txtDiagLabel6 = new wxStaticText(this, wxID_ANY, "Server NAK:               ", wxPoint(50, 300), wxSize(100, 20));
	txtDiagLabel7 = new wxStaticText(this, wxID_ANY, "Server Busy:              ", wxPoint(50, 350), wxSize(100, 20));
	txtDiagLabel8 = new wxStaticText(this, wxID_ANY, "Character Overrun:        ", wxPoint(50, 400), wxSize(100, 20));

	//Initialize diagnostic counters
	txtDiag1 = new wxStaticText(this, wxID_ANY, wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[0]), wxPoint(340, 50), wxSize(50, 20), wxALIGN_RIGHT);
	txtDiag2 = new wxStaticText(this, wxID_ANY, wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[1]), wxPoint(340, 100), wxSize(50, 20), wxALIGN_RIGHT);
	txtDiag3 = new wxStaticText(this, wxID_ANY, wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[2]), wxPoint(340, 150), wxSize(50, 20), wxALIGN_RIGHT);
	txtDiag4 = new wxStaticText(this, wxID_ANY, wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[3]), wxPoint(340, 200), wxSize(50, 20), wxALIGN_RIGHT);
	txtDiag5 = new wxStaticText(this, wxID_ANY, wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[4]), wxPoint(340, 250), wxSize(50, 20), wxALIGN_RIGHT);
	txtDiag6 = new wxStaticText(this, wxID_ANY, wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[5]), wxPoint(340, 300), wxSize(50, 20), wxALIGN_RIGHT);
	txtDiag7 = new wxStaticText(this, wxID_ANY, wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[6]), wxPoint(340, 350), wxSize(50, 20), wxALIGN_RIGHT);
	txtDiag8 = new wxStaticText(this, wxID_ANY, wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[7]), wxPoint(340, 400), wxSize(50, 20), wxALIGN_RIGHT);

	//Initialize error message text
	txtDiagResetError = new wxStaticText(this, wxID_ANY, _T("Błąd w resetowaniu liczników Modbus"), wxPoint(90, 510), wxSize(80, 10));
	txtDiagResetError->Show();

	//Set diagnostics font
	myFont = wxFont(11, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
	if (txtDiagLabel1 != nullptr) txtDiagLabel1->SetFont(myFont);
	if (txtDiagLabel2 != nullptr) txtDiagLabel2->SetFont(myFont);
	if (txtDiagLabel3 != nullptr) txtDiagLabel3->SetFont(myFont);
	if (txtDiagLabel4 != nullptr) txtDiagLabel4->SetFont(myFont);
	if (txtDiagLabel5 != nullptr) txtDiagLabel5->SetFont(myFont);
	if (txtDiagLabel6 != nullptr) txtDiagLabel6->SetFont(myFont);
	if (txtDiagLabel7 != nullptr) txtDiagLabel7->SetFont(myFont);
	if (txtDiagLabel8 != nullptr) txtDiagLabel8->SetFont(myFont);

	myFont = wxFont(11, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
	if (txtDiag1 != nullptr) txtDiag1->SetFont(myFont);
	if (txtDiag2 != nullptr) txtDiag2->SetFont(myFont);
	if (txtDiag3 != nullptr) txtDiag3->SetFont(myFont);
	if (txtDiag4 != nullptr) txtDiag4->SetFont(myFont);
	if (txtDiag5 != nullptr) txtDiag5->SetFont(myFont);
	if (txtDiag6 != nullptr) txtDiag6->SetFont(myFont);
	if (txtDiag7 != nullptr) txtDiag7->SetFont(myFont);
	if (txtDiag8 != nullptr) txtDiag8->SetFont(myFont);

	//Set error message font
	myFont = wxFont(9, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
	if (txtDiagResetError != nullptr) txtDiagResetError->SetFont(myFont);
	if (txtDiagResetError != nullptr) txtDiagResetError->SetForegroundColour(wxColour(255, 0, 0));

	if (txtDiagResetError != nullptr) txtDiagResetError->Hide();

	if (pMain->mbStatusDiagnostics == -1)
	{
		if (txtDiag1 != nullptr) txtDiag1->SetLabel("N/A");
		if (txtDiag2 != nullptr) txtDiag2->SetLabel("N/A");
		if (txtDiag3 != nullptr) txtDiag3->SetLabel("N/A");
		if (txtDiag4 != nullptr) txtDiag4->SetLabel("N/A");
		if (txtDiag5 != nullptr) txtDiag5->SetLabel("N/A");
		if (txtDiag6 != nullptr) txtDiag6->SetLabel("N/A");
		if (txtDiag7 != nullptr) txtDiag7->SetLabel("N/A");
		if (txtDiag8 != nullptr) txtDiag8->SetLabel("N/A");

		btnReset->Disable(); //disable reset button
	}

	//Create Modbus diagnostics reset timer
	mbDiagnosticsResetTimer = new wxTimer(this, 10006);

	StartDiagnosticsThread();

	//Re-enable menu item
	if (!(pMain->mToolsMenu->IsEnabled(wxID_PREFERENCES))) pMain->mToolsMenu->Enable(wxID_PREFERENCES, true);
}

/* "Reset" button callback */
void cDiagnostics::OnButtonClickedReset(wxCommandEvent& evt)
{
	if (btnReset->IsEnabled()) btnReset->Disable(); //disable reset button
	pMain->mbDiagnosticsResetEnabled = true; //signal Modbus thread to reset counters
	pMain->mbDiagnosticsResetting = true; //wait for Modbus thread to indicate reset message has been sent
	mbDiagnosticsResetPressed = true;
}

/* Start diagnostics helper thread */
void cDiagnostics::StartDiagnosticsThread()
{
	if (CreateThread(wxTHREAD_JOINABLE) != wxTHREAD_NO_ERROR)
	{
		wxLogError("Could not create the worker thread!");
		return;
	}
	if (GetThread()->Run() != wxTHREAD_NO_ERROR)
	{
		wxLogError("Could not run the worker thread!");
		return;
	}
}

/* Diagnostics helper thread */
wxThread::ExitCode cDiagnostics::Entry()
{
	while (!GetThread()->TestDestroy())
	{
		wxThread::Sleep(MB_SCAN_RATE);
		wxQueueEvent(this, new wxThreadEvent());
	}
	return (wxThread::ExitCode)0;
}

/* Diagnostics helper thread event handler */
void cDiagnostics::OnThreadUpdate(wxThreadEvent& evt)
{
	wxCriticalSectionLocker lock(csLock);

	//Check if diagnostics have been reset
	if (mbDiagnosticsResetPressed && !(pMain->mbDiagnosticsResetting))
	{
        //Check for errors
        if ((pMain->mbStatusDiagnosticsReset == -1))
        {
            txtDiagResetError->Show();
            if (mbDiagnosticsResetTimer->IsRunning()) mbDiagnosticsResetTimer->Stop();
            mbDiagnosticsResetTimer->Start(1500, wxTIMER_ONE_SHOT);
        }

        //Re-enable reset button
        if (!(btnReset->IsEnabled())) btnReset->Enable();

        //Clear flag
        mbDiagnosticsResetPressed = false;
	}

	if (!(pMain->mbDiagnosticsResetting))
	{
		if (pMain->mbStatusDiagnostics == -1) //communication error
		{
			txtDiag1->SetLabel("N/A");
			txtDiag2->SetLabel("N/A");
			txtDiag3->SetLabel("N/A");
			txtDiag4->SetLabel("N/A");
			txtDiag5->SetLabel("N/A");
			txtDiag6->SetLabel("N/A");
			txtDiag7->SetLabel("N/A");
			txtDiag8->SetLabel("N/A");

			if (btnReset->IsEnabled()) btnReset->Disable(); //disable reset button
		}
		else //update diagnostics
		{
			txtDiag1->SetLabel(wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[0]));
			txtDiag2->SetLabel(wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[1]));
			txtDiag3->SetLabel(wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[2]));
			txtDiag4->SetLabel(wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[3]));
			txtDiag5->SetLabel(wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[4]));
			txtDiag6->SetLabel(wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[5]));
			txtDiag7->SetLabel(wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[6]));
			txtDiag8->SetLabel(wxString::Format(wxT("%i"), pMain->mbDiagnosticsRegister[7]));

			if ((!mbDiagnosticsResetPressed) && (!mbDiagnosticsResetTimer->IsRunning()))
			{
				if (!(btnReset->IsEnabled())) btnReset->Enable(); //re-enable reset button
			}
		}
	}
}

/* Timer event - diagnostics reset error */
void cDiagnostics::OnDiagnosticsResetTimer(wxTimerEvent& evt)
{
	txtDiagResetError->Hide();
}

/* Diagnostics frame closed */
void cDiagnostics::OnClose(wxCloseEvent& evt)
{
	//Stop timer
	if (mbDiagnosticsResetTimer->IsRunning()) mbDiagnosticsResetTimer->Stop();

	//Delete helper thread
	if (GetThread() && GetThread()->IsRunning()) GetThread()->Delete();

	pMain->mbDiagnosticsWindowIsOpen = false; //signal Modbus thread that diagnostics window is no longer open
	pMain->mbDiagnosticsEnabled = false; //signal Modbus thread to stop reading diagnostics
	pMain->mbDiagnosticsAvailable = false;

	Destroy();
}
