#include "cApp.h"

wxIMPLEMENT_APP(cApp);

cApp::cApp()
{
	//Initialize variables and pointers
    mFrame = nullptr;
    wxChecker = nullptr;
    mbSlaveNum = 0;
    mbAddress = 0;
    mbPortNum = 0;
    str2numTemp = 0;
}

bool cApp::OnInit()
{
    wxApp::SetAppName(wxString(_T("Pomiar Wiązki")));

	{
		wxLogNull logNo; //suppress information message
		wxChecker = new wxSingleInstanceChecker(_T(".pomiarwiazki_temp"));
	}

	//Check if process already exists
	if (wxChecker->IsAnotherRunning())
	{
		if (wxMessageBox(_T("\nProces już istnieje.\nZakończyć poprzedni proces?"),
				_T("Pomiar Wiązki"), wxOK | wxCANCEL | wxICON_INFORMATION) == wxOK)
		{
			//Get PID of previous instance
			string command = "kill $(pidof -o " + to_string(::getpid()) + " PomiarWiazki)";
			int32_t rtn = system(command.c_str());

			//Delete old single instance checker
			command = "rm " + wxString::FromUTF8(getenv("HOME")).ToStdString() + "/.pomiarwiazki_temp";
			rtn = system(command.c_str());
			rtn = rtn + 1; //to prevent compiler warnings

			//Create new single instance checker
			wxLogNull logNo; //suppress information message
			delete wxChecker;
			wxChecker = new wxSingleInstanceChecker(_T(".pomiarwiazki_temp"));
		}
		else
		{
			delete wxChecker;
			wxChecker = nullptr;
			return false;
		}
	}

	//Open configuration file
	tFilename = wxString::FromUTF8(getenv("HOME")) + "/konfiguracja.txt";
	tFile.Open(tFilename);

	//Read configuration file
	while (!tFile.Eof())
	{
		strConfig = tFile.GetNextLine();

		if ((strConfig[0] != '#') && !(strConfig.IsEmpty()) && (mbSlaveNum <= MB_MAX_CONNECTIONS))
		{
			if (strConfig.Mid(0, 12) == wxString("Adres Modbus"))
			{
                mbSlaveNum++;
				mbAddressStr = strConfig.Mid(16);
				mbAddressStr.ToLong(&str2numTemp);
				mbAddress = (int32_t)str2numTemp;
			}
			else if (strConfig.Mid(0, 14) == wxString("Port Szeregowy"))
			{
			    mbPort = strConfig.Mid(16, 10);

			    for (uint8_t i = 0; i < mbPortNum; i++)
			    {
			        if (mbPort == mbPortHistory[i])
			        {
			            mbPort = wxString("Port Error");
			        }
			    }

			    mbPortHistory[mbPortNum++] = mbPort;
			}
			else if (strConfig.Mid(0, 5) == wxString(_T("Tytuł")))
			{
				mFrameTitle = strConfig.Mid(7);

				//Create main frame
				mFrame = new cMain(&mbPort, mbAddress, mbSlaveNum, &mFrameTitle);
				mFrame->SetIcon(wxICON(icon));
				mFrame->Show();
			}
		}
	}

	return true;
}

int cApp::OnExit()
{
    delete wxChecker;
    wxChecker = nullptr;
    return 0;
}
