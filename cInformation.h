#pragma once

#include "wx/wx.h"
#include "cMain.h"

class cMain;

class cInformation : public wxFrame
{
public: //constructor
	cInformation(cMain*);

private: //event handlers
	void OnClose(wxCloseEvent& evt);

private: //pointers received in contructor
	cMain* pMain;

private: //variables and pointers
    wxFont myFont;
    wxStaticText* txtInformation;

	wxDECLARE_EVENT_TABLE();
};

