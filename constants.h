#ifndef CONSTANTS_H_
#define CONSTANTS_H_

//Buffer sizes
const uint8_t MB_NUM_COILS = 3;
const uint8_t MB_PORT_CHAR_LENGTH = 10;
const uint8_t MB_NUM_ADC_REG = 5;
const uint8_t MB_NUM_DIAG = 8;
const uint8_t MB_DIAG_RAW_LENGTH = 6;

//Modbus configuration
const int  MB_MAX_CONNECTIONS = 10;
const int  MB_BAUD = 19200;
const char MB_PARITY = 'E';
const int  MB_NUM_BITS = 8;
const int  MB_NUM_STOP = 1;
const uint64_t MB_SCAN_RATE = 100;

//ADC scaling and reference value tolerance levels
const int32_t ADC_REFERENCE_MIN = 105;
const int32_t ADC_REFERENCE_MAX = 125;
const float ADC_SCALING_FACTOR = 131.67;
const float ADC_SCALING_FACTOR_CORRECTED = 254.8;

//Timer values
const int BUTTON_TIMEOUT = 1250;
const int MOTOR_TIMEOUT = 3000;

#endif
