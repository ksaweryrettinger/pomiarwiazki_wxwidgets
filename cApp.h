#pragma once

#include <iostream>
#include <string.h>
#include "Graphics/icon.xpm"
#include "modbus.h"
#include "wx/wx.h"
#include "wx/log.h"
#include "wx/textfile.h"
#include "wx/snglinst.h"
#include "cMain.h"
#include "constants.h"

using namespace std;

class cApp : public wxApp
{
public:
	cApp();

public:
    virtual bool OnInit() override;
    virtual int OnExit() override;

private:
    wxSingleInstanceChecker* wxChecker;
	cMain* mFrame;
    wxString mFrameTitle;

private: //Configuration file variables
	wxTextFile tFile;
	wxString tFilename;
	wxString strConfig;
    int64_t str2numTemp;

private: //Modbus configuration
    uint8_t mbPortNum;
    wxString mbPort;
    wxString mbPortHistory[MB_MAX_CONNECTIONS];
    wxString mbSlaveNumStr;
    wxString mbAddressStr;
    uint8_t mbSlaveNum;
    int32_t mbAddress;
};

